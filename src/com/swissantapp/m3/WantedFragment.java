package com.swissantapp.m3;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

public class WantedFragment extends PostListBaseFragment {
  public final static String URL = "http://www.englishforum.ch/external.php?type=RSS2&forumid=14";

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = super.onCreateView(inflater, container, savedInstanceState);
    new Feed(URL, this, getActivity()).fetch();
    return v;
  }
}
