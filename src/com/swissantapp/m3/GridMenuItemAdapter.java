package com.swissantapp.m3;

import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ImageView;

public class GridMenuItemAdapter extends BaseAdapter {
  private final Context mCtxt;

  public GridMenuItemAdapter(Context ctxt) {
    mCtxt = ctxt;
  }

  @Override
  public int getCount() {
    return MENU_ITEMS.length;
  }

  @Override
  public View getView(int pos, View convertView, ViewGroup par) {
    View v = convertView;
    if(v == null) {
      LayoutInflater inflater = LayoutInflater.from(mCtxt);
      v = inflater.inflate(R.layout.grid_menu_item, null);
      TextView tv = (TextView) v.findViewById(R.id.menu_caption);
      tv.setText(MENU_ITEMS[pos][0]);
      ImageView iv = (ImageView) v.findViewById(R.id.menu_icon);
      iv.setImageResource(MENU_ITEMS[pos][1]);
    } 
    return v;
  }

  private static final Integer[][] MENU_ITEMS = {
    { R.string.offered, android.R.drawable.ic_menu_view },
    { R.string.wanted, android.R.drawable.ic_menu_view },
    { R.string.new_offered_post, android.R.drawable.ic_menu_add },
    { R.string.new_wanted_post, android.R.drawable.ic_menu_add },
    { R.string.my_posts, android.R.drawable.ic_menu_manage }};

  @Override
  public Object getItem(int pos) { return null; }

  @Override
  public long getItemId(int pos) { return 0; }
}
