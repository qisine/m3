package com.swissantapp.m3;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import com.swissantapp.m3.db.PostDbHelper;

public class Db {
  private final SQLiteOpenHelper mDbHelper;
  private SQLiteDatabase mRDb;
  private SQLiteDatabase mWDb;

  public Db(Context ctx) {
    mDbHelper = new PostDbHelper(ctx);
  }

  private SQLiteDatabase getReadableDatabase() {
    if(mRDb == null) mRDb = mDbHelper.getReadableDatabase();
    return mRDb;
  }

  private SQLiteDatabase getWritableDatabase() {
    if(mWDb == null) mWDb = mDbHelper.getWritableDatabase();
    return mWDb;
  }

  public Cursor readAll() {
    return readById(null);
  }

  public Cursor readById(int id) { return readById(new Integer(id)); }
  public Cursor readById(Integer id) {
    String[] cols = 
      { Post.PostEntry._ID, Post.PostEntry.TITLE, Post.PostEntry.BODY, Post.PostEntry.CREATED_DATE };
    String sel = null;
    String[] selArgs = null;
    if(id != null) {
      sel =  "WHERE id = ?";
      selArgs = new String[] { id.toString() };
    }

    SQLiteDatabase db = getReadableDatabase();
    return db.query(Post.PostEntry.TABLE_NAME, cols, sel, selArgs, null, null, null);
  }

  public void write(Post m) {
    if(m == null || m.getTitle() == null && m.getBody() == null) return;
    SQLiteDatabase db = getWritableDatabase();
    db.insert(Post.PostEntry.TABLE_NAME, null, m.toContentValues());        
  }

  public void writeAll(List<Post> posts) {
    if(posts == null || posts.size() < 1) return;
    for(Post p: posts) 
      write(p);
  }
}
