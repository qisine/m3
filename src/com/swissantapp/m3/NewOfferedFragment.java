package com.swissantapp.m3;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

public class NewOfferedFragment extends NewPostBaseFragment {
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.new_offered_post, container, false);
  }

  @Override
  public void onSubmit() {

  }
}
