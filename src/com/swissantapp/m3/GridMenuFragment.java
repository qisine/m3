package com.swissantapp.m3;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.GridView;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class GridMenuFragment extends Fragment {
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    GridView gv = (GridView) inflater.inflate(R.layout.grid_menu, null);
    gv.setAdapter(new GridMenuItemAdapter(getActivity()));

    List<Class<Fragment>> clsses = new ArrayList<Class<Fragment>>();
    for(Class c: MainActivity.MENU_FRAGMENT_CLASSES)
      clsses.add(c);

    gv.setOnItemClickListener(new MainActivity.GridMenuListener<Fragment>(
                                    getActivity(),
                                    clsses));
    return gv;
  }
}
