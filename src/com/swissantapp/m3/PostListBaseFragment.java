package com.swissantapp.m3;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.LinearLayout;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.content.Intent;

import java.util.List;
import java.util.ArrayList;

import com.swissantapp.m3.feed.OnTaskCompleted;

public abstract class PostListBaseFragment extends Fragment implements OnTaskCompleted {
  private ArrayAdapter<Post> mAdpt;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);

    List<Post> posts = new ArrayList<Post>();

    mAdpt = new ArrayAdapter<Post>(
      getActivity(),
      android.R.layout.simple_list_item_1,
      android.R.id.text1,
      posts);

    LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.post_list, container, false);
    ListView lv = (ListView) ll.findViewById(R.id.posts); 
    lv.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> par, View v, int pos, long id) {
        Post p = (Post) par.getItemAtPosition(pos);
        Intent i = new Intent(getActivity(), DetailsActivity.class);
        i.putExtra(Post.class.getName(), p);
        startActivity(i);
      }
    });

    lv.setAdapter(mAdpt);
    return ll;
  }

  @Override
  public void onTaskCompleted(List<Post> posts) {
    mAdpt.addAll(posts);
  }
}
