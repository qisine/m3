package com.swissantapp.m3;

import android.content.Context;

import java.util.List;

import com.swissantapp.m3.feed.*;

public class Feed extends android.os.AsyncTask<String, Integer, List<Post>> {
  private final String mUrl;
  private final OnTaskCompleted mListener;
  private final Db mDb;

  public Feed(String url, OnTaskCompleted listener, Context ctx) {
    mUrl = url;
    mListener = listener;
    mDb = new Db(ctx);
  }

  public void fetch() {
    execute(mUrl);
  }

  @Override
  protected List<Post> doInBackground(String... urls) {
    List<Post> posts = new FeedParser(urls[0]).parse();
    return posts;
  }

  @Override 
  protected void onProgressUpdate(Integer... progress) { }

  @Override
  protected void onPostExecute(List<Post> posts) {
    mListener.onTaskCompleted(posts);
  }
}

