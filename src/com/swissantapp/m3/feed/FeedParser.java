package com.swissantapp.m3.feed;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException ;
import java.util.Scanner;

import android.util.Xml;
import android.sax.Element;
import android.sax.RootElement;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;

import com.swissantapp.m3.Post;

public class FeedParser {

  static final String PUB_DATE = "pubDate";
  static final  String DESCRIPTION = "description";
  static final  String LINK = "link";
  static final  String TITLE = "title";
  static final  String ITEM = "item";

  final URL feedUrl;

  public FeedParser(String feedUrl){
    try {
      this.feedUrl = new URL(feedUrl);
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  public List<Post> parse() {
    final Post currentPost = new Post();
    RootElement root = new RootElement("rss");
    final List<Post> posts = new ArrayList<Post>();
    Element channel = root.getChild("channel");
    Element item = channel.getChild(ITEM);
    item.setEndElementListener(new EndElementListener(){
        public void end() {
          posts.add(currentPost.copy());
        }
        });
    item.getChild(TITLE).setEndTextElementListener(new EndTextElementListener(){
        public void end(String title) {
          currentPost.setTitle(title);
        }
        });
    item.getChild(LINK).setEndTextElementListener(new EndTextElementListener(){
        public void end(String link) {
          currentPost.setLink(link);
        }
        });
    item.getChild(DESCRIPTION).setEndTextElementListener(new 
        EndTextElementListener(){
        public void end(String body) {
          currentPost.setBody(body);
        }
        });
    item.getChild(PUB_DATE).setEndTextElementListener(new EndTextElementListener(){
        public void end(String dt) {
          currentPost.setCreatedDate(dt);
        }
        });
    try {
      Xml.parse(getInputStream(), Xml.Encoding.UTF_8, 
          root.getContentHandler());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return posts;
  }

  private InputStream getInputStream() {
    try {
      String s = new Scanner(feedUrl.openConnection().getInputStream(), "GBK").useDelimiter("\\A").next();
      return new ByteArrayInputStream(s.getBytes());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
