package com.swissantapp.m3.feed;

import com.swissantapp.m3.Post;

public interface OnTaskCompleted {
  void onTaskCompleted(java.util.List<Post> posts); 
}
