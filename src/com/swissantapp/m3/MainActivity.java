package com.swissantapp.m3;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Window;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.List;
import java.util.ArrayList;

public class MainActivity extends Activity {
  private Fragment mMenuGridFrag;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    
    FragmentTransaction ft = getFragmentManager().beginTransaction();
    if(mMenuGridFrag == null) {
      Fragment f = Fragment.instantiate(this, GridMenuFragment.class.getName());
      mMenuGridFrag = f;
      ft.replace(android.R.id.content, mMenuGridFrag);
    } else {
      ft.attach(mMenuGridFrag);
    }
    ft.addToBackStack("menu_grid_frag").commit();
  }

  public static class GridMenuListener<T extends Fragment> implements AdapterView.OnItemClickListener {
    private final List<Fragment> mFrags = new ArrayList<Fragment>();
    private Fragment mCurrFrag;
    private final Activity mAct;
    private final List<String> mTags;
    private final List<Class<T>> mClasses;

    public GridMenuListener(Activity a, List<Class<T>> classes) {
      mAct = a;
      mClasses = classes;
      mTags = new ArrayList<String>();
      for(Class c: mClasses) 
        mTags.add(c.getName());
    }

    @Override
    public void onItemClick(AdapterView<?> par, View v, int pos, long id) {
      FragmentTransaction ft = mAct.getFragmentManager().beginTransaction();
      if(mCurrFrag != null) ft.detach(mCurrFrag);

      mCurrFrag = null;
      if (mFrags.size() > pos) mCurrFrag = mFrags.get(pos);
      if (mCurrFrag == null) {
        mCurrFrag = Fragment.instantiate(mAct, mClasses.get(pos).getName());
        mFrags.add(mCurrFrag);
        ft.replace(android.R.id.content, mCurrFrag, mTags.get(pos));
      } else {
        ft.attach(mCurrFrag);
      }
      ft.addToBackStack("child_view").commit();
    }
  }

  public static final Class[] MENU_FRAGMENT_CLASSES =
    new Class[] { OfferedFragment.class,
                  WantedFragment.class,
                  NewOfferedFragment.class,
                  NewWantedFragment.class,
                  MyPostsFragment.class };
}
