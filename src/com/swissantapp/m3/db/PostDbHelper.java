package com.swissantapp.m3.db;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class PostDbHelper extends SQLiteOpenHelper {
  public static final int DATABASE_VERSION = 1;
  public static final String DATABASE_NAME = "posts.db";

  private SQLiteDatabase mDb;

  public static final String SQL_CREATE_TABLE =
    "CREATE TABLE " + Post.PostEntry.TABLE_NAME + "("
      + Post.PostEntry._ID + " INTEGER PRIMARY KEY, "
      + Post.PostEntry.TITLE + " TEXT, "
      + Post.PostEntry.BODY + " TEXT)";

  public static final String SQL_DROP_TABLE = 
    "DROP TABLE IF EXISTS " + Post.PostEntry.TABLE_NAME; 

  public PostDbHelper(Context ctx) {
    super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(SQL_DROP_TABLE);
    db.execSQL(SQL_CREATE_TABLE);
    //seedTable(db);
    mDb = db;
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onCreate(db);
  }

  @Override
  public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onUpgrade(db, oldVersion, newVersion);
  }

  private static void seedTable(SQLiteDatabase db) {
    db.execSQL("INSERT INTO " + Post.PostEntry.TABLE_NAME + "(title, body) "
    + "values('foo', 'bar baz bat'), ('nana', 'ni nosonw8sl8l<F3'), ('xyz', 'asdfasdfasdf')");
  }
}
