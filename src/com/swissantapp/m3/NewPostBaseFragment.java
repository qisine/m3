package com.swissantapp.m3;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import java.util.List;

import com.swissantapp.m3.feed.*;

public abstract class NewPostBaseFragment extends Fragment implements OnTaskCompleted {
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  public void onSubmit() {
  }

  @Override
  public void onTaskCompleted(List<Post> posts) {
  }
}
