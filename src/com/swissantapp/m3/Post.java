package com.swissantapp.m3;

import android.provider.BaseColumns;
import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Date;

public class Post implements Parcelable, Comparable<Post> {
    static SimpleDateFormat FORMATTER = 
        new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
    private String title;
    private URL link;
    private String body;
    private Date created_date;

    public URL getLink() { return this.link; }
    public void setLink(String link) {
        try {
            this.link = new URL(link);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getBody() { return this.body; }
    public void setBody(String d) { this.body = d; }

    public String getTitle() { return this.title; }
    public void setTitle(String t) { this.title = t; }

    public String getCreatedDate() {
        return FORMATTER.format(this.created_date);
    }

    public void setCreatedDate(String date) {
        // pad the date if necessary
        while (!date.endsWith("00")){
            date += "0";
        }
        try {
            created_date = FORMATTER.parse(date.trim());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Post copy() {
      Post m = new Post();
      m.setLink(link.toString());
      m.setCreatedDate(getCreatedDate());
      m.setTitle(getTitle());
      m.setBody(getBody());
      return m;
    }

    @Override
    public String toString() { return getTitle(); }

    @Override
    public int compareTo(Post another) {
        if (another == null) return 1;
        return another.getCreatedDate().compareTo(created_date.toString());
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel out, int flags) {
      out.writeString(title);
      out.writeString(link.toString());
      out.writeString(body);
      out.writeString(getCreatedDate());
    }

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
      @Override
      public Post createFromParcel(Parcel in) {
        Post m = new Post();
        m.setTitle(in.readString());
        m.setLink(in.readString());
        m.setBody(in.readString());
        m.setCreatedDate(in.readString());
        return m;
      }

      @Override
      public Post[] newArray(int size) {
        return new Post[size];
      }
    };

    public ContentValues toContentValues() {
      ContentValues cv = new ContentValues();
      cv.put("title", title);
      cv.put("body", body);
      cv.put("created_date", getCreatedDate());
      return cv;
    }

  public static class PostEntry implements BaseColumns {
    public static final String TABLE_NAME = "posts";
    public static final String TITLE = "title";
    public static final String BODY = "body";
    public static final String CREATED_DATE = "created_date";
  }

    //@Override
    //public int hashCode() { // omitted for brevity }
    
    /*@Override
    public boolean equals(Object obj) {
            // omitted for brevity
    }*/

}
