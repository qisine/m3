package com.swissantapp.m3;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import java.util.List;

public class DetailsActivity extends Activity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.details);

    Post p = (Post) getIntent().getParcelableExtra(Post.class.getName());

    ((TextView) findViewById(R.id.url)).setText(p.getLink().toString());
    ((TextView) findViewById(R.id.title)).setText(p.getTitle());
    ((TextView) findViewById(R.id.body)).setText(p.getBody());
  }
}

