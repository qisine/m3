package com.swissantapp.m3;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.Context;

public class Utils {
  public static boolean isNetworkConnected(Context ctx) {
    ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo ni = cm.getActiveNetworkInfo();
    if(ni != null && ni.isConnectedOrConnecting()) return true;
    return false;
  }
}

